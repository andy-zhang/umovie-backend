## API

APIVERSION : /v1

MOTHED|PATH|DESC|
------|----|----|
GET|/get/movie/info/:name|name:电影名称,如果存在特殊字符，建议进行URL编码|
POST|/get/movie/down|{"url":"电影地址"}|
POST|/get/movie/cili|{"url":"磁力地址"}|

## Example

### 

```
curl -X GET http://127.0.0.1:8000/v1/get/movie/info/大话西游

curl -X GET 127.0.0.1:8000/v1/get/movie/info/%e5%a4%a7%e8%af%9d%e8%a5%bf%e6%b8%b8
```

### 
```
curl -X POST \
  http://127.0.0.1:8000/v1/get/movie/down \
  -d '{
	"url":"/movie/da-hua-xi-you-zhi-da-sheng-qu-qin/"
}'
```

###
```
curl -X POST \
  http://127.0.0.1:8000/v1/get/movie/cili \
  -d '{
	"url":"http://www.cilisoba.net/h/86ae4a955aeb339bd27bd9ff887ed6e62c09e5b4?fr=fm"
}'
```

