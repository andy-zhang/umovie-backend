package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

var _VERSION_ = "unknown"

const (
	APIV1           = "/v1"
	GETMOVIEINFO    = "/get/movie/info/:name"
	GETMOVIEDOWNURL = "/get/movie/down"
	GETCILIDOWNURL  = "/get/movie/cili"
	ERROR           = 500
)

type DownInfo struct {
	URL string `json:"url"`
}

func main() {
	fmt.Println(getVersion())

	router := httprouter.New()
	router.GET(getAPIPath(GETMOVIEINFO), _getMovieInfo)
	router.POST(getAPIPath(GETMOVIEDOWNURL), _getMovieDown)
	router.POST(getAPIPath(GETCILIDOWNURL), _getCiLiUrl)
	handler := cors.Default().Handler(router)
	log.Fatal(http.ListenAndServe(":8000", handler))
}

func getVersion() string {
	return _VERSION_
}

// getAPIPath 返回指定版本的API endpoint
func getAPIPath(path string) string {
	log.Println(APIV1 + path)
	return APIV1 + path
}

func _getMovieInfo(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	name := p.ByName("name")
	if name == "" {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, "Parameter Error!")
		return
	}
	log.Println(name)
	info, err := GetMovieInfo(name)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	log.Println(info)

	data, err := json.Marshal(info)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(data))
	return
}

func _getMovieDown(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	var dl DownInfo

	err = json.Unmarshal(content, &dl)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	mdl, err := ParseMovieURL(dl.URL, "real")
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	tmdl, err := ParseMovieURL(dl.URL, "magnet")
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	mdl = append(mdl, tmdl...)

	respon, err := json.Marshal(mdl)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(respon))
	return

}

func _getCiLiUrl(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	var dl DownInfo

	err = json.Unmarshal(content, &dl)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	curl, err := GenerateCiLi(dl.URL)
	if err != nil {
		w.WriteHeader(ERROR)
		fmt.Fprintf(w, err.Error())
		return
	}

	log.Println("curl = ", curl)
	// cl := DownInfo{
	// 	URL: curl,
	// }

	// respon, err := json.Marshal(cl)
	// if err != nil {
	// 	w.WriteHeader(ERROR)
	// 	fmt.Fprintf(w, err.Error())
	// 	return
	// }

	// w.Header().Set("Content-Type", "application/json")
	// fmt.Fprintf
	fmt.Fprint(w, curl)
	// log.Printf("[%s]\n", string(respon))
	return
}
