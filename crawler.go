package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type MovieInfo struct {
	Name string `json:"name"`
	Img  string `json:"img"`
	// Year     string `json:"year"`
	Rating string `json:"rating"`
	// Director string `json:"director"`
	// Actor    string `json:"actor"`
	Href string `json:"href"`
}

type MovieDownURL struct {
	// 0在线播放 1磁力下载
	Type   int    `json:"type"`
	URL    string `json:"url"`
	Name   string `json:"name"`
	Source string `json:"source"`
}

type DocHtml struct {
	Count int    `json:"count"`
	Html  string `json:"html"`
}

type CiLi struct {
	Result []CiLiData `json:"result"`
}

type CiLiData struct {
	InfoHash string `json:"info_hash"`
	Name     string `json:"name"`
}

// src: 'http://7xksqe.com1.z0.glb.clouddn.com/media/poster/129/1292213.jpg-poster100',
// name: '大话西游之大圣娶亲',
// year: 1995,
// rating: "豆瓣 9.1 / IMDB 8.1",
// director: "刘镇伟",
// actor: "周星驰 / 吴孟达 / 朱茵 / 蔡少芬 / 蓝洁瑛",
const (
	MOVIESEARCHAPI = "http://dianying.fm/search/?text=%s"
	MOVIEDOWNAPI   = "http://dianying.fm%splaylinks/%s"
	CILIDOWNAPI    = "http://www.cilisoba.net/api/json_info?hashes=%s"
	REFERER        = "http://dianying.fm%s"
)

// GetMovieInfo 根据电影名称检索相关电影信息
//  name 电影名称
func GetMovieInfo(name string) ([]MovieInfo, error) {
	var vs []MovieInfo
	searchRequest := fmt.Sprintf(MOVIESEARCHAPI, url.QueryEscape(name))
	log.Println(searchRequest)
	doc, err := goquery.NewDocument(searchRequest)
	if err != nil {
		return vs, err
	}

	doc.Find(".fm-result-list").Each(func(i int, s *goquery.Selection) {
		// log.Println("=========")
		s.Find("li").Each(func(i int, s *goquery.Selection) {
			movie := MovieInfo{}
			movie.Img, _ = s.Find(".fm-movie-cover").Find("a").Find("img").Attr("src")
			s.Find(".fm-movie-title").Each(func(i int, s *goquery.Selection) {
				// 获取电影名称
				// movie.Name = s.Find("a").Text()
				// 获取电影链接
				movie.Href, _ = s.Find("a").Attr("href")
				// movie.Img, _ = s.Find("a").Find("img").Attr("src")
				// log.Println(s.Find("a").Html())
			})

			// 获取电影名称
			movie.Name = strings.TrimSpace(s.Find(".fm-movie-title").Text())
			movie.Name = strings.Replace(movie.Name, " ", "", -1)
			// 获取电影评分
			s.Find(".fm-rating").Each(func(i int, s *goquery.Selection) {
				s.Find("a").Each(func(i int, s *goquery.Selection) {
					if s.Text() != "" {
						if movie.Rating != "" {
							movie.Rating = movie.Rating + " / " + strings.TrimSpace(s.Text())
						} else {
							movie.Rating = strings.TrimSpace(s.Text())
						}
					}
				})
			})

			if movie.Name != "" && movie.Href != "" {
				vs = append(vs, movie)
			}

			// // 获取导演
			// s.Find(".div").Each(func(i int, s *goquery.Selection) {
			// 	if s.Find("line-title").Text() == "" {

			// 	}
			// })
		})

	})

	return vs, nil
}

// ParseMovieURL 解析电影播放地址和下载地址
//  url 电影链接
// ty 检索类型 在线播放/磁力链接
func ParseMovieURL(url string, ty string) ([]MovieDownURL, error) {

	var md []MovieDownURL
	realAPI := fmt.Sprintf(MOVIEDOWNAPI, url, ty)
	log.Println(realAPI)

	client := &http.Client{}
	req, err := http.NewRequest("GET", realAPI, nil)
	if err != nil {
		return md, err
	}
	referer := fmt.Sprintf(REFERER, url)
	req.Header.Add("Referer", referer)
	resp, err := client.Do(req)
	if err != nil {
		return md, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return md, err
	}

	var dh DocHtml

	err = json.Unmarshal(content, &dh)
	if err != nil {
		return md, err
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(dh.Html))
	if err != nil {
		return md, err
	}

	// log.Println(doc.Html())
	// log.Println(doc.Text())
	// log.Println(doc.Text())
	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		if _, ok := s.Attr("rel"); ok {
			down := MovieDownURL{}
			switch ty {
			case "real":
				down.Type = 0
			case "magnet":
				down.Type = 1
			}

			href, _ := s.Attr("href")

			down.URL = href
			md = append(md, down)
		}
	})

	log.Println(len(md))

	// 解析HTML获取片名，来源
	var tempMovie []string
	tempMovie = strings.Split(doc.Text(), "立即下载")
	if len(tempMovie) == 1 {
		tempMovie = strings.Split(doc.Text(), "立即播放")
	}

	for i, movie := range tempMovie {
		mv := []rune(strings.Replace(strings.Replace(movie, "\n", "+", -1), " ", "", -1))
		// log.Printf("MV=[%s]\n", mv)
		idx := 0
		sdxStart := 0
		sdxEnd := 0
		ndxStart := 0
		ndxEnd := 0
		for n, s := range mv {
			// 43 +
			if n+1 <= len(mv) {
				// log.Printf("== %d [%c][%s] [%v] \n", n, s, mv[n+1:n+2], (s != 43 && mv[n+1] == 43))
				if s != 43 && mv[n+1] == 43 {
					idx++
					if idx == 3 {
						sdxEnd = n
					}
					if idx == 4 {
						ndxEnd = n
						break
					}
				}
				// if idx == 1 {
				// 	if s == 43 && mv[n+1] != 43 {
				// 		log.Printf("idx=1 %d [%c][%s]", n+1, s, mv[n+1:n+10])
				// 		// sdxStart = n + 1
				// 	}
				// }

				if idx == 2 {
					if s == 43 && mv[n+1] != 43 {
						// log.Printf("idx=2 %d [%c][%s]", n+1, s, mv[n+1:n+10])
						sdxStart = n + 1
					}
				}

				if idx == 3 {
					if s == 43 && mv[n+1] != 43 {
						ndxStart = n + 1
					}
				}
			}
		}
		// log.Println(sdxStart, sdxEnd, ndxStart, ndxEnd)
		if ndxStart != 0 && ndxEnd != 0 && sdxStart != 0 && sdxEnd != 0 {
			md[i].Name = string(mv[ndxStart : ndxEnd+1])
			md[i].Source = string(mv[sdxStart : sdxEnd+1])
		}
	}
	return md, nil
}

// GenerateCiLi 生成磁力链接字符串
// ucili 磁力链接地址
func GenerateCiLi(ucili string) (string, error) {
	downurl := ""
	client := &http.Client{CheckRedirect: func(req *http.Request, via []*http.Request) error {
		// log.Printf("In the CheckRedirect [%v]", req.Header)
		return http.ErrUseLastResponse
	}}
	req, err := http.NewRequest("GET", ucili, nil)
	if err != nil {
		return downurl, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return downurl, err
	}

	// data, _ := ioutil.ReadAll(resp.Body)

	// log.Println(string(data))
	location := resp.Header.Get("location")
	if location == "" {
		// 找不到磁力地址
		return downurl, fmt.Errorf("找不到磁力地址")
	}
	log.Println(location)
	hash := strings.Split(location, "/")
	hashcode := hash[len(hash)-1]
	if _, err := strconv.Atoi(hashcode); err != nil {
		// 磁力地址获取失败
		return downurl, err
	}

	curl := fmt.Sprintf(CILIDOWNAPI, hashcode)
	req, err = http.NewRequest("GET", curl, nil)
	if err != nil {
		return downurl, err
	}

	resp, err = client.Do(req)
	if err != nil {
		return downurl, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return downurl, err
	}

	var cl CiLi

	err = json.Unmarshal(content, &cl)
	if err != nil {
		return downurl, err
	}

	log.Println(cl.Result[0].Name)
	magnet := fmt.Sprintf("magnet:?xt=urn:btih:%s&dn=%s", cl.Result[0].InfoHash, url.QueryEscape(cl.Result[0].Name))
	log.Printf("magnet = [%s]", magnet)
	return magnet, nil
}
